ARG GO_VERSION=1
FROM golang:${GO_VERSION}-bookworm as builder

WORKDIR /go/src/app
COPY . .
RUN go mod download && go mod verify
RUN go build -v -o /bin/app ./cmd

FROM debian:bookworm

RUN apt-get update && apt-get install -y ca-certificates
RUN update-ca-certificates

COPY --from=builder /go/src/app/public /goapp/public
COPY --from=builder /go/src/app/dist /goapp/dist

WORKDIR /goapp
COPY --from=builder /bin/app /goapp/app
CMD ["/goapp/app"]
