package api

type Author struct {
	Id       int
	Name     string
	Surname  string
	Nickname string
}

func (a *Author) HasNickname() bool {
	return a.Nickname != ""
}
