package api

import (
	"strings"
	"time"
)

type Game struct {
	Id        int
	IgdbId        int
	Title     string
	Developer *Developer
	Publisher *Publisher
	Cover string
	ReleaseDate time.Time
	Developers []string
	Publishers []string
	Genres []string
	Platforms []string
	Screenshots []string
}

func NewGame() *Game {
	return &Game{
		Genres: make([]string, 0),
	}
}

func (g *Game) HasPub() bool {
	return g.Publisher != nil
}

func (g *Game) HasDev() bool {
	return g.Developer != nil
}

func (g *Game) GetGenres() string {
	return buildArrStr(g.Genres)
}

func (g *Game) GetDevs() string {
	return buildArrStr(g.Developers)
}

func (g *Game) GetPubs() string {
	return buildArrStr(g.Publishers)
}

func buildArrStr(arr []string) string {
	var b strings.Builder
	for _, s := range arr {
		if b.String() != "" {
			b.WriteString(", ")
		}
		b.WriteString(s)
	}
	return b.String()
}
