package api

import (
	"fmt"
	"time"
)

type Magazine struct {
	Id          int
	Number      int
	PublishDate string
	Price       float32
	URL         string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

func (m *Magazine) PublishDateFmt() string {
	if m.PublishDate == "" {
		return ""
	}

	month := m.PublishDate[:2]
	year := m.PublishDate[2:]

	return fmt.Sprintf("%s %s", getMonthName(month), year)
}

func getMonthName(month string) string {
	if month == "" {
		return ""
	}

	switch month {
	case "01":
		return "Gennaio"
	case "02":
		return "Febbraio"
	case "03":
		return "Marzo"
	case "04":
		return "Aprile"
	case "05":
		return "Maggio"
	case "06":
		return "Giugno"
	case "07":
		return "Luglio"
	case "08":
		return "Agosto"
	case "09":
		return "Settembre"
	case "10":
		return "Ottobre"
	case "11":
		return "Novembre"
	case "12":
		return "Dicembre"
	}

	return ""
}
