package api

type Review struct {
	Id       int
	Game     *Game
	Magazine *Magazine
	Author   *Author
	Page     int
	Score    int
	URL      string
}
