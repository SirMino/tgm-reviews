package main

import (
	"flag"

	"github.com/joho/godotenv"
	"github.com/labstack/gommon/log"
	"mi.no/tgm-reviews/internal/server"
)

func main() {

	dev := flag.Bool("dev", false, "Pass this flag to run in dev mode locally")
	flag.Parse()

	if *dev {
		if err := godotenv.Load(); err != nil {
			log.Fatal(err)
		}
	}

	s, err := server.CreateServer(*dev)
	if err != nil {
		log.Fatal(err)
	}

	s.Start()
}
