const closeModal = (event) => {
	event.preventDefault();
	const modal = document.getElementById("modal-game");
	if (!modal) return;
	modal.close();
};
// // Close with a click outside
// document.addEventListener("click", (event) => {
//   if (visibleModal === null) return;
//   const modalContent = visibleModal.querySelector("article");
//   const isClickInside = modalContent.contains(event.target);
//   !isClickInside && closeModal(visibleModal);
// });
//
// // Close with Esc key
// document.addEventListener("keydown", (event) => {
//   if (event.key === "Escape" && visibleModal) {
//     closeModal(visibleModal);
//   }
// });
