package controllers

import (
	"fmt"
	"log"
	"sync"
	"time"

	"mi.no/tgm-reviews/api"
	"mi.no/tgm-reviews/internal/data"
	"mi.no/tgm-reviews/internal/db"
	"mi.no/tgm-reviews/internal/igdb"
)

func GetReviewLight(db db.Storage, id int) (api.Review, error) {
	var r api.Review

	rr, err := db.GetReviewByGame(id)
	if err != nil {
		return r, err
	}
	gr, err := db.GetGameRecord(id)
	if err != nil {
		return r, err
	}
	if !gr.IgdbId.Valid {
		gid, err := igdb.SearchGame(gr.Title.String)
		if err != nil {
			return r, err
		}
		go db.UpdateIgdbId(int(gr.Id.Int64), gid)

		gr.IgdbId.Int64 = int64(gid)
	}

	r.Author = rr.Author
	r.Score = rr.Score
	r.Page = rr.Page
	r.URL = rr.URL
	r.Magazine = rr.Magazine
	r.Game = &api.Game{
		Id: int(gr.Id.Int64),
		IgdbId: int(gr.IgdbId.Int64),
		Title: gr.Title.String,
	}

	return r, nil
}

func GetReview(db db.Storage, id int) (api.Review, error) {

	fmt.Println("Start GetReview controller")

	// load data from db
	// load data from igdb
	// build api game struct
	// return struct to render
	var r api.Review

	rr, err := db.GetReviewByGame(id)
	if err != nil {
		return r, err
	}
	gr, err := db.GetGameRecord(id)
	if err != nil {
		return r, err
	}
	g, err := buildGame(gr)
	if err != nil {
		return r, err
	}

	r.Author = rr.Author
	r.Score = rr.Score
	r.Page = rr.Page
	r.URL = rr.URL
	r.Magazine = rr.Magazine
	r.Game = &g

	fmt.Println("End GetReview controller")
	return r, nil
}

func GetGameDetails(igdbId int) (*api.Game, error) {

	game := api.NewGame()

	gi, err := igdb.GetGameInfo(igdbId)
	if err != nil {
		fmt.Println(err.Error())
	}
	if len(gi) > 0 {
		gif := gi[0]
		game.Title = gif.Name
		game.ReleaseDate = time.Unix(int64(gif.FirstReleaseDate), 0)

		var wg sync.WaitGroup

		// game cover
		game.Cover = igdb.GetImageUrl(igdb.CoverBig, gif.Cover.ImageID)

		// game genres
		wg.Add(1)
		go func() {
			defer wg.Done()
			genIDs := make([]int, 0)
			for _, g := range gif.Genres {
				genIDs = append(genIDs, g.ID)
			}
			gen, err := igdb.GetGameGenres(genIDs)
			if err != nil {
				log.Printf("Error getting game genre: %s\n", err.Error())
			}
			for _, g := range gen {
				game.Genres = append(game.Genres, g.Name)
			}
		}()

		// Companies
		wg.Add(1)
		go func() {
			defer wg.Done()
			for _, c := range gif.InvolvedCompanies {
				cinfo, err := igdb.GetCompanyInfo(c.Company)
				if err != nil {
					fmt.Printf("Error getting company data: %s\n", err.Error())
				}
				if c.Developer {
					game.Developers = append(game.Developers, cinfo[0].Name)
				}
				if c.Publisher {
					game.Publishers = append(game.Publishers, cinfo[0].Name)
				}
			}
		}()

		// Platforms and logos
		wg.Add(1)
		go func() {
			defer wg.Done()
			// pLogosIds := make([]int, 0)
			for _, p := range gif.Platforms {
				// pLogosIds = append(pLogosIds, p.PlatformLogo)
				switch {
				case p.Abbreviation == "PC":
					game.Platforms = append(game.Platforms, "fa-windows")
				case p.Abbreviation == "Linux":
					game.Platforms = append(game.Platforms, "fa-linux")
				case p.Abbreviation == "Mac":
					game.Platforms = append(game.Platforms, "fa-apple")
				case p.Abbreviation[:2] == "PS":
					game.Platforms = append(game.Platforms, "fa-playstation")
				case p.Abbreviation[:1] == "X":
					game.Platforms = append(game.Platforms, "fa-xbox")
				}
			}
			// logos, err := igdb.GetPlatformsLogos(pLogosIds)
			// if err != nil {
			// 	log.Printf("Error getting game plaforms: %s\n", err.Error())
			// }
			// for _, l := range logos {
			// 	game.Platforms = append(game.Platforms, igdb.GetImageUrl(igdb.LogoMed, l.ImageID))
			// }
		}()

		wg.Wait()

	}
	
	return game, nil
}

func buildGame(g data.GameRecord) (api.Game, error) {

	fmt.Println("Start buildGame")

	var game api.Game

	if !g.IgdbId.Valid {
		gid, err := igdb.SearchGame(g.Title.String)
		if err != nil {
			return game, err
		}
		g.IgdbId.Int64 = int64(gid)
	}

	gi, err := igdb.GetGameInfo(int(g.IgdbId.Int64))
	if err != nil {
		fmt.Println(err.Error())
	}
	if len(gi) > 0 {
		gif := gi[0]
		game.Title = gif.Name
		game.ReleaseDate = time.Unix(int64(gif.FirstReleaseDate), 0)

		// game cover
		game.Cover = igdb.GetImageUrl(igdb.CoverBig, gif.Cover.ImageID)

		// game genres
		genIDs := make([]int, 0)
		for _, g := range gif.Genres {
			genIDs = append(genIDs, g.ID)
		}
		gen, err := igdb.GetGameGenres(genIDs)
		if err != nil {
			log.Printf("Error getting game genre: %s\n", err.Error())
		}
		for _, g := range gen {
			game.Genres = append(game.Genres, g.Name)
		}

		// Companies
		for _, c := range gif.InvolvedCompanies {
			cinfo, err := igdb.GetCompanyInfo(c.Company)
			if err != nil {
				fmt.Printf("Error getting company data: %s\n", err.Error())
			}
			if c.Developer {
				game.Developers = append(game.Developers, cinfo[0].Name)
			}
			if c.Publisher {
				game.Publishers = append(game.Publishers, cinfo[0].Name)
			}
		}

		// Platforms and logos
		pLogosIds := make([]int, 0)
		for _, p := range gif.Platforms {
			pLogosIds = append(pLogosIds, p.PlatformLogo)
			switch {
			case p.Abbreviation == "PC":
				game.Platforms = append(game.Platforms, "fa-windows")
			case p.Abbreviation == "Linux":
				game.Platforms = append(game.Platforms, "fa-linux")
			case p.Abbreviation == "Mac":
				game.Platforms = append(game.Platforms, "fa-apple")
			case p.Abbreviation[:2] == "PS":
				game.Platforms = append(game.Platforms, "fa-playstation")
			case p.Abbreviation[:1] == "X":
				game.Platforms = append(game.Platforms, "fa-xbox")
			}
		}
		// logos, err := igdb.GetPlatformsLogos(pLogosIds)
		// if err != nil {
		// 	log.Printf("Error getting game plaforms: %s\n", err.Error())
		// }
		// for _, l := range logos {
		// 	game.Platforms = append(game.Platforms, igdb.GetImageUrl(igdb.LogoMed, l.ImageID))
		// }
	}
	
	fmt.Println("End buildGame")
	return game, nil
}
