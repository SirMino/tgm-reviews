package data

import "database/sql"

type GameRecord struct {
	Id sql.NullInt64
	Title sql.NullString
	DevId sql.NullInt64
	PubId sql.NullInt64
	IgdbId sql.NullInt64
}
