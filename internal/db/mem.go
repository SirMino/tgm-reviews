package db

import (
	"fmt"
	"strings"
	"time"

	"mi.no/tgm-reviews/api"
)

type MemStorage struct {
	games     []api.Game
	magazines []api.Magazine
	reviews   []api.Review
	authors   []api.Author
	devs      []api.Developer
	pubs      []api.Publisher
}

func (s *MemStorage) SearchGame(search string) ([]api.Game, error) {
	r := make([]api.Game, 0)
	lowSearch := strings.ToLower(search)
	for _, g := range s.games {
		lowTitle := strings.ToLower(g.Title)
		if strings.Contains(lowTitle, lowSearch) {
			r = append(r, g)
		}
	}
	return r, nil
}

func (s *MemStorage) GetGames() ([]api.Game, error) {
	return s.games, nil
}
func (s *MemStorage) GetGame(id int) (api.Game, error) {
	for _, g := range s.games {
		if g.Id == id {
			return g, nil
		}
	}

	return api.Game{}, fmt.Errorf("No game found")
}

func (s *MemStorage) GetReviewByGame(id int) (api.Review, error) {
	for _, rr := range s.reviews {
		if rr.Game.Id == id {
			return rr, nil
		}
	}

	return api.Review{}, fmt.Errorf("No review found")

}
func (s *MemStorage) GetMagazine(id int) (api.Magazine, error) {
	for _, m := range s.magazines {
		if m.Id == id {
			return m, nil
		}
	}
	return api.Magazine{}, fmt.Errorf("No magazine found")
}
func (s *MemStorage) GetAuthor(id int) (api.Author, error) {
	for _, a := range s.authors {
		if a.Id == id {
			return a, nil
		}
	}
	return api.Author{}, fmt.Errorf("No author found")
}

func (s *MemStorage) Close() error {
	fmt.Println("Mem storage closed")
	return nil
}

func NewMemStorage() *MemStorage {

	var m = &MemStorage{
		games:     make([]api.Game, 0),
		magazines: make([]api.Magazine, 0),
		reviews:   make([]api.Review, 0),
		devs:      make([]api.Developer, 0),
		pubs:      make([]api.Publisher, 0),
	}

	m.devs = append(m.devs, api.Developer{
		Id:   1,
		Name: "Gianpippo dev",
	})
	m.devs = append(m.devs, api.Developer{
		Id:   2,
		Name: "dev Asdrubale",
	})

	m.pubs = append(m.pubs, api.Publisher{
		Id:   1,
		Name: "Bart",
	})
	m.pubs = append(m.pubs, api.Publisher{
		Id:   12,
		Name: "TigroTime",
	})

	// GAMES -----------------------------
	m.games = append(m.games, api.Game{
		Id:        0,
		Title:     "Sandstorm Striker",
		// Image:     "https://1.bp.blogspot.com/-RmUTncpcykE/TWK3-XGLyHI/AAAAAAAAAE8/eoAlfxZCEG0/s1600/star-wars.jpg",
		Developer: &m.devs[0],
		Publisher: &m.pubs[1],
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
	})
	m.games = append(m.games, api.Game{
		Id:    1,
		Title: "Mirage Mech",
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
	})
	m.games = append(m.games, api.Game{
		Id:    2,
		Title: "Astral Outlaw",
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
	})
	m.games = append(m.games, api.Game{
		Id:        3,
		Title:     "Dust Devil Destroyer",
		Developer: &m.devs[1],
		Publisher: &m.pubs[1],
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
	})
	m.games = append(m.games, api.Game{
		Id:    4,
		Title: "Solar Swashbuckler",
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
	})
	m.games = append(m.games, api.Game{
		Id:        5,
		Title:     "Sea Breeze Ranch",
		Developer: &m.devs[1],
		Publisher: &m.pubs[0],
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
	})
	m.games = append(m.games, api.Game{
		Id:    6,
		Title: "Sands of Past",
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
	})

	// MAGAZINES -----------------------------
	m.magazines = append(m.magazines, api.Magazine{
		Id:          0,
		Number:      12,
		PublishDate: "102010",
		Price:       8.9,
	})
	m.magazines = append(m.magazines, api.Magazine{
		Id:          1,
		Number:      65,
		PublishDate: "032012",
		Price:       8.9,
	})
	m.magazines = append(m.magazines, api.Magazine{
		Id:          2,
		Number:      33,
		PublishDate: "012011",
		Price:       8.9,
	})
	m.magazines = append(m.magazines, api.Magazine{
		Id:          3,
		Number:      24,
		PublishDate: "152011",
		Price:       8.9,
	})

	// AUTHORS --------------------------------
	m.authors = append(m.authors, api.Author{
		Id:       0,
		Name:     "John",
		Surname:  "Field",
		Nickname: "Beast",
	})
	m.authors = append(m.authors, api.Author{
		Id:       1,
		Name:     "Nick",
		Surname:  "Carter",
		Nickname: "BBoy",
	})
	m.authors = append(m.authors, api.Author{
		Id:       2,
		Name:     "Micheal",
		Surname:  "Springfield",
		Nickname: "--^Thao^--",
	})
	m.authors = append(m.authors, api.Author{
		Id:       3,
		Name:     "Roger",
		Surname:  "Donalg",
		Nickname: "Glover",
	})
	m.authors = append(m.authors, api.Author{
		Id:       4,
		Name:     "Momo",
		Surname:  "Just",
		Nickname: "Beefang",
	})
	m.authors = append(m.authors, api.Author{
		Id:       5,
		Name:     "Reginald",
		Surname:  "Milord",
		Nickname: "_Owl Ears_",
	})

	// REVIEWS --------------------------------
	// for i := range 7 {
	// 	mag := rand.Intn(3)
	// 	auth := rand.Intn(5)
	// 	page := rand.Intn(150)
	// 	score := rand.Intn(100)
	// 	m.reviews = append(m.reviews, api.Review{
	// 		Id:       i,
	// 		Game:     &m.games[i],
	// 		Magazine: &m.magazines[mag],
	// 		Author:   &m.authors[auth],
	// 		Page:     page,
	// 		Score:    score,
	// 	})
	// }

	return m
}

func newTime(val string) time.Time {
	t, _ := time.Parse("20060102", val)
	return t
}
