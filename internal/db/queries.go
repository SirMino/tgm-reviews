package db

import (
	"database/sql"
	"fmt"

	"mi.no/tgm-reviews/api"
)

func searchGames(db *sql.DB, str string) ([]api.Game, error) {
	results := make([]api.Game, 0)
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM games WHERE title LIKE '%%%s%%'", str))
	if err != nil {
		return results, err
	}
	defer rows.Close()

	for rows.Next() {
		var id sql.NullInt64
		var title sql.NullString
		var dev_id sql.NullInt64
		var pub_id sql.NullInt64
		if err := rows.Scan(&id, &title, &dev_id, &pub_id); err != nil {
			return results, err
		}

		var g api.Game
		if id.Valid {
			g.Id = int(id.Int64)
		}
		if title.Valid {
			g.Title = title.String
		}
		if dev_id.Valid {
		}
		if pub_id.Valid {
		}
		results = append(results, g)
	}
	return results, nil
}
