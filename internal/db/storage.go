package db

import (
	"mi.no/tgm-reviews/api"
	"mi.no/tgm-reviews/internal/data"
)

type Storage interface {
	Sync() error
	SearchGame(search string) ([]api.Game, error)
	GetGames() ([]api.Game, error)
	GetGame(id int) (api.Game, error)
	GetGameRecord(id int) (data.GameRecord, error)
	UpdateIgdbId(id, igdbid int) error
	GetReviewByGame(gameId int) (api.Review, error)
	GetMagazine(id int) (api.Magazine, error)
	GetAuthor(id int) (api.Author, error)
	NewAuthor(a api.Author) error
	GetUserPwd(username string) (string, error)
	Close() error
}
