package db

import (
	"database/sql"
	"fmt"
	"log/slog"
	"os"
	"path"

	"github.com/tursodatabase/go-libsql"
	"mi.no/tgm-reviews/api"
	"mi.no/tgm-reviews/internal/data"
)

type TursoDB struct {
	db        *sql.DB
	connector *libsql.Connector
}

func NewTursoDB(dev bool) (*TursoDB, error) {
	var lsql TursoDB
	var dbName string
	dbFile := os.Getenv("SQLITEFILE")
	if dev {
		dbName = fmt.Sprintf("file:./%s", dbFile)
	} else {
		dbUrl := os.Getenv("SQLITEURL")
		tursoToken := os.Getenv("TURSOTOKEN")

		tmpDir, err := os.MkdirTemp("", "*-db")
		if err != nil {
			return &lsql, err
		}

		dbPath := path.Join(tmpDir, dbFile)
		connector, err := libsql.NewEmbeddedReplicaConnector(dbPath, dbUrl, libsql.WithAuthToken(tursoToken))
		if err != nil {
			return &lsql, err
		}
		// defer connector.Close()

		db := sql.OpenDB(connector)
		lsql.db = db
		lsql.connector = connector
		return &lsql, nil
		// dbName = fmt.Sprintf("%s?authToken=%s", dbUrl, tursoToken)
	}
	db, err := sql.Open("libsql", dbName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to open db %s", err)
		return &lsql, err
	}

	lsql.db = db

	return &lsql, nil
}

func (s *TursoDB) Sync() error {
	return s.connector.Sync()
}

func (s *TursoDB) SearchGame(search string) ([]api.Game, error) {

	results := make([]api.Game, 0)
	// results2, _ := searchGames(s.db, search)
	// fmt.Printf("results2 len %d\n", len(results2))

	// rows, err := s.db.Query(fmt.Sprintf("SELECT * FROM games WHERE title LIKE '%%%s%%'", search))
	rows, err := s.db.Query("SELECT * FROM games WHERE title LIKE ?", "%"+search+"%")
	if err != nil {
		return results, err
	}
	defer rows.Close()

	for rows.Next() {
		var id sql.NullInt64
		var title sql.NullString
		var dev_id sql.NullInt64
		var pub_id sql.NullInt64
		var xxx_id sql.NullInt64
		if err := rows.Scan(&id, &title, &dev_id, &pub_id, &xxx_id); err != nil {
			return results, err
		}

		var g api.Game
		if id.Valid {
			g.Id = int(id.Int64)
		}
		if title.Valid {
			g.Title = title.String
		}
		if dev_id.Valid {
		}
		if pub_id.Valid {
		}
		results = append(results, g)
	}

	return results, nil
}
func (s *TursoDB) GetGames() ([]api.Game, error) {
	return []api.Game{}, nil
}
func (s *TursoDB) GetGame(id int) (api.Game, error) {

	var g api.Game
	row := s.db.QueryRow("SELECT * FROM games WHERE id = ?", id)
	var title sql.NullString
	var devId sql.NullInt64
	var pubId sql.NullInt64
	var xxx_id sql.NullInt64
	if err := row.Scan(&id, &title, &devId, &pubId, &xxx_id); err != nil {
		return g, err
	}

	g.Id = id
	if title.Valid {
		g.Title = title.String
	}
	if devId.Valid {
		d, err := s.GetDeveloper(int(devId.Int64))
		if err != nil {
			slog.Error(err.Error())
		}
		g.Developer = &d
	}
	if pubId.Valid {
		p, err := s.GetPublisher(int(pubId.Int64))
		if err != nil {
			slog.Error(err.Error())
		}
		g.Publisher = &p
	}
	return g, nil
}

func (s *TursoDB) GetGameRecord(id int) (data.GameRecord, error) {

	var g data.GameRecord

	row := s.db.QueryRow("SELECT id, title, igdb_id FROM games WHERE id = ?", id)
	if err := row.Scan(&g.Id, &g.Title, &g.IgdbId); err != nil {
		return g, err
	}

	return g, nil
}

func (s *TursoDB) UpdateIgdbId(id, igdbid int) error {

	upStr := "UPDATE games SET igdb_id=? WHERE id=?"
	_, err := s.db.Exec(upStr, igdbid, id)
	if err != nil {
		return err
	}
	return s.Sync()
}

func (s *TursoDB) GetReviewByGame(gameId int) (api.Review, error) {

	var r api.Review

	var id sql.NullInt64
	var game_id sql.NullInt64
	var magazine_id sql.NullInt64
	var author_id sql.NullInt64
	var page sql.NullInt64
	var score sql.NullInt64
	var url sql.NullString

	row := s.db.QueryRow("SELECT * FROM reviews WHERE game_id = ?", gameId)
	if err := row.Scan(&id, &game_id, &magazine_id, &author_id, &page, &score, &url); err != nil {
		return r, err
	}

	if id.Valid {
		r.Id = int(id.Int64)
	}
	g, err := s.GetGame(gameId)
	if err != nil {
		slog.Error(err.Error())
	}
	r.Game = &g
	if magazine_id.Valid {
		m, err := s.GetMagazine(int(magazine_id.Int64))
		if err != nil {
			slog.Error(err.Error())
		}
		r.Magazine = &m
	}
	if author_id.Valid {
		a, err := s.GetAuthor(int(author_id.Int64))
		if err != nil {
			slog.Error(err.Error())
		}
		r.Author = &a
	}
	if page.Valid {
		r.Page = int(page.Int64)
	}
	if score.Valid {
		r.Score = int(score.Int64)
	}
	if url.Valid {
		r.URL = url.String
	}

	return r, nil
}
func (s *TursoDB) GetMagazine(id int) (api.Magazine, error) {

	var m api.Magazine

	var number sql.NullInt64
	var publishDate sql.NullString
	var price sql.NullFloat64
	var url sql.NullString

	row := s.db.QueryRow("SELECT * FROM magazines WHERE id = ?", id)
	if err := row.Scan(&m.Id, &number, &publishDate, &price, &url); err != nil {
		return m, err
	}
	m.Id = id
	if number.Valid {
		m.Number = int(number.Int64)
	}
	if publishDate.Valid {
		m.PublishDate = publishDate.String
	}
	if price.Valid {
		m.Price = float32(price.Float64)
	}
	if url.Valid {
		m.URL = url.String
	}

	return m, nil
}
func (s *TursoDB) GetAuthor(id int) (api.Author, error) {

	var a api.Author

	var name sql.NullString
	var surname sql.NullString
	var nickname sql.NullString

	row := s.db.QueryRow("SELECT * FROM authors WHERE id = ?", id)
	if err := row.Scan(&a.Id, &name, &surname, &nickname); err != nil {
		return a, err
	}

	a.Id = id
	if name.Valid {
		a.Name = name.String
	}
	if surname.Valid {
		a.Surname = surname.String
	}
	if nickname.Valid {
		a.Nickname = nickname.String
	}

	return a, nil
}

func (s *TursoDB) NewAuthor(a api.Author) error {

	stmt, err := s.db.Prepare("INSERT INTO authors (name, surname, nickname) VALUES (?,?,?)")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(a.Name, a.Surname, a.Nickname)
	if err != nil {
		return err
	}

	return nil
}

func (s *TursoDB) GetDeveloper(id int) (api.Developer, error) {

	var d api.Developer

	var name sql.NullString

	row := s.db.QueryRow("SELECT * FROM developers WHERE id = ?", id)
	if err := row.Scan(&d.Id, name); err != nil {
		return d, err
	}

	if name.Valid {
		d.Name = name.String
	}

	return d, nil
}

func (s *TursoDB) GetPublisher(id int) (api.Publisher, error) {

	var p api.Publisher

	var name sql.NullString

	row := s.db.QueryRow("SELECT * FROM publishers WHERE id = ?", id)
	if err := row.Scan(&p.Id, name); err != nil {
		return p, err
	}

	if name.Valid {
		p.Name = name.String
	}

	return p, nil
}

func (s *TursoDB) GetUserPwd(username string) (string, error) {
	row := s.db.QueryRow("SELECT username, password FROM users WHERE username = ?", username)
	var uname, pwd string
	if err := row.Scan(&uname, &pwd); err != nil {
		return "", err
	}
	return pwd, nil
}

func (s *TursoDB) Close() error {
	fmt.Println("turso close")
	if s.connector != nil {
		s.connector.Close()
	}
	return s.db.Close()
}
