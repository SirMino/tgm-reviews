package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"mi.no/tgm-reviews/api"
	"mi.no/tgm-reviews/internal/db"
)

func NewAuthorForm(c echo.Context) error {
	return c.Render(http.StatusOK, "new-author", nil)
}

func NewAuthor(c echo.Context) error {
	name := c.FormValue("name")
	nname := c.FormValue("nickname")
	sname := c.FormValue("surname")
	db := c.Get("__db").(db.Storage)
	err := db.NewAuthor(api.Author{
		Name:     name,
		Surname:  sname,
		Nickname: nname,
	})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// no need to call sync, write is always remote-first
	// if err := db.Sync(); err != nil {
	// 	return c.JSON(http.StatusInternalServerError, err.Error())
	// }

	return c.NoContent(http.StatusNoContent)
}
