package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"mi.no/tgm-reviews/api"
	"mi.no/tgm-reviews/internal/controllers"
	"mi.no/tgm-reviews/internal/db"
	"mi.no/tgm-reviews/internal/igdb"
)

type UserData struct {
	Name string
	Pwd string
}

func newUserData(name, pwd string) UserData {
	return UserData {
		Name: name,
		Pwd: pwd,
	}
}

type RootData struct {
	User UserData
}

func newRootData(user UserData) RootData {
	return RootData{
		User: user,
	}
}

func Root(c echo.Context) error {
	sess, _ := session.Get("session", c)
	if sess.Values["user"] != nil {
		fmt.Println("hell Ive got a user in session here")
		return c.Render(http.StatusOK, "index", newRootData(newUserData("myuser", "mypwd")))
		// TODO check user value
	} else {
		fmt.Println("oh boy, no user in session, yaknow?")
	}
	return c.Render(http.StatusOK, "index", nil)
}

func Search(c echo.Context) error {
	str := c.QueryParam("src")
	// if str == "" || len(str) <= 2 {
	if str == "" {
		return c.String(http.StatusNoContent, "")
	}
	db := c.Get("__db").(db.Storage)
	gs, err := db.SearchGame(str)
	if err != nil {
		return err
	}
	rs := make([]api.Review, 0)
	for _, g := range gs {
		r, _ := db.GetReviewByGame(g.Id)
		rs = append(rs, r)
	}
	return c.Render(http.StatusOK, "cards", rs)
}

func Game(c echo.Context) error {
	db := c.Get("__db").(db.Storage)
	gIdStr := c.QueryParam("gameId")
	if gIdStr == "" {
		return c.String(http.StatusBadRequest, "No Game ID")
	}
	gId, err := strconv.Atoi(gIdStr)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	r, err := db.GetReviewByGame(gId)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	return c.Render(http.StatusOK, "gamemodal", r)
}

func GameCover(c echo.Context) error {

	gIdStr := c.QueryParam("gameId")
	if gIdStr == "" {
		return c.String(http.StatusBadRequest, "No Game ID")
	}
	gId, err := strconv.Atoi(gIdStr)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	info, _ := igdb.GetGameInfo(gId)

	cvr, err := igdb.GetGameCover(info[0].Cover.ID)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	if len(cvr) > 0 {
	img := igdb.GetImageUrl(igdb.CoverBig, cvr[0].ImageID)

	return c.HTML(http.StatusOK, fmt.Sprintf("<img src=\"%s\">", img))
	}
	return nil
}

func GameDetails(c echo.Context) error {
	idStr := c.QueryParam("IgdbId")
	if idStr == "" {
		return c.String(http.StatusBadRequest, "No IGDB Game ID")
	}
	gId, err := strconv.Atoi(idStr)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	details, _ := controllers.GetGameDetails(gId)
	// details, _ := igdb.GetGameInfo(gId)

	return c.Render(http.StatusOK, "gamedetails", details)
}
