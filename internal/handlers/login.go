package handlers

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo-contrib/session"
	"golang.org/x/crypto/bcrypt"
	"mi.no/tgm-reviews/internal/db"
)

func LoginForm(c echo.Context) error {
	return c.Render(http.StatusOK, "login-form", nil)
}

func Login(c echo.Context) error {
	user := c.FormValue("username")
	pwd := c.FormValue("password")
	db := c.Get("__db").(db.Storage)
	keys := c.Get("__keys").([][]byte)
	// store := c.Get("__store").(*sessions.CookieStore)

	if user != "" && pwd != "" {

		// hashed, err := bcrypt.GenerateFromPassword([]byte(pwd), 8)
		// if err != nil {
		// 	log.Printf("error hashing %s\n", err.Error())
		// 	return c.String(http.StatusInternalServerError, err.Error())
		// }
		dbPwd, err := db.GetUserPwd(user)
		if err != nil {
			log.Printf("error getting user pwd %s\n", err.Error())
			return c.String(http.StatusNotFound, err.Error())
		}

		if err := bcrypt.CompareHashAndPassword([]byte(dbPwd), []byte(pwd)); err != nil {
			log.Printf("error comparing pwd %s\n", err.Error())
			return c.String(http.StatusUnauthorized, err.Error())
		}

		// should return a cookie to the frontend for 
		uid := securecookie.GenerateRandomKey(32)
		keys = append(keys, uid)

		// coo := new(http.Cookie)
		// coo.Name = "uid"
		// coo.Value = string(uid)
		// coo.Expires = time.Now().Add(5 * time.Minute)
		// c.SetCookie(coo)

		sess, _ := session.Get("session", c)
		sess.Options = &sessions.Options{
			Path: "/",
			MaxAge: 86400 * 7,
			HttpOnly: true,
		}
		sess.Values["user"] = user
		if err := sess.Save(c.Request(), c.Response()); err != nil {
			return fmt.Errorf("Cannot save session: %w", err)
		}

		// s, err := store.Get(c.Request(), "tr-reviews")
		// if err != nil {
		// 	return c.String(http.StatusInternalServerError, err.Error())
		// }
		// s.Values["uid"] = uid
		// s.Save(c.Request(), c.Response().Writer)

		fmt.Println("Login OK")
	}
	return c.Render(http.StatusOK, "dashboard", nil)
	// return c.String(http.StatusOK, "")
}
