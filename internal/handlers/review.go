package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"mi.no/tgm-reviews/internal/controllers"
	"mi.no/tgm-reviews/internal/db"
)
func GetReview(c echo.Context) error {
	return getReviewLight(c)
}

func getReview(c echo.Context) error {
	fmt.Println("Start GetReview handler")
	db := c.Get("__db").(db.Storage)
	gIdStr := c.QueryParam("gameId")
	if gIdStr == "" {
		return c.String(http.StatusBadRequest, "No Game ID")
	}
	gId, err := strconv.Atoi(gIdStr)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	r, err := controllers.GetReview(db, gId)
	// r, err := db.GetReviewByGame(gId)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	fmt.Println("End GetReview handler")
	return c.Render(http.StatusOK, "reviewmodal", r)
}

func getReviewLight(c echo.Context) error {
	db := c.Get("__db").(db.Storage)
	gIdStr := c.QueryParam("gameId")
	if gIdStr == "" {
		return c.String(http.StatusBadRequest, "No Game ID")
	}
	gId, err := strconv.Atoi(gIdStr)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	// r, err := db.GetReviewByGame(gId)
	r, err := controllers.GetReviewLight(db, gId)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.Render(http.StatusOK, "reviewlight", r)
}
