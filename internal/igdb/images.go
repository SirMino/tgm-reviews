package igdb

import "fmt"

type ImageType int
const (
	CoverSmall ImageType = iota
	ScreenshotMed
	CoverBig
	LogoMed
	ScreenshotBig
	ScreenshotHuge
	Thumb
	Micro
	P720
	P1080
)

func (i ImageType) String() string {
	switch i {
	case CoverSmall: return "cover_small"
	case ScreenshotMed: return "screenshot_med"
	case CoverBig: return "cover_big"
	case LogoMed: return "logo_med"
	case ScreenshotBig: return "screenshot_big"
	case ScreenshotHuge: return "screenshot_huge"
	case Thumb: return "thumb"
	case Micro: return "micro"
	case P720: return "720p"
	case P1080: return "1080p"
	default: return "thumb"
	}
}

const url string = "https://images.igdb.com/igdb/image/upload/t_%s/%s.png"

func GetImageUrl(t ImageType, id string) string {
	return fmt.Sprintf(url, t.String(), id)
}
