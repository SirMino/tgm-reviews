package igdb

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

const (
	gamesUrl = "https://api.igdb.com/v4/games"
	companyUrl = "https://api.igdb.com/v4/companies"
)

func SearchGame(name string) (int, error) {
	var g GameSearch
	body := []byte(fmt.Sprintf("search \"%s\"; fields first_release_date,cover;where version_parent=null;", name))
	resp, err := runRequest(gamesUrl, body)
	if err != nil {
		return 0, fmt.Errorf("Error while searching games: %s", err.Error())
	}
	if err := json.Unmarshal(resp, &g); err != nil {
		return 0, fmt.Errorf("Error unmarshaling searching games info response: %w", err)
	}

	rDate := 0
	for _, gi := range g {
		if rDate == 0 {
			rDate = gi.FirstReleaseDate
		} else {
			if rDate > gi.FirstReleaseDate {
				rDate = gi.FirstReleaseDate
			}
		}
	}
	for _, gi := range g {
		if rDate == gi.FirstReleaseDate {
			return gi.ID, nil
		}
	}

	return 0, nil
}

func GetGameInfo(igdbId int) (GameInfo, error) {
	var gi GameInfo
	body := []byte(fmt.Sprintf("fields name, first_release_date, platforms.abbreviation, platforms.name, platforms.platform_logo, cover.image_id, cover.url, involved_companies.company, involved_companies.developer, involved_companies.publisher, genres.name, screenshots.image_id, screenshots.url; where id = %d;", igdbId))
	resp, err := runRequest(gamesUrl, body)
	if err != nil {
		return gi, fmt.Errorf("Error while getting game info: %s", err.Error())
	}
	if err := json.Unmarshal(resp, &gi); err != nil {
		return gi, fmt.Errorf("Error unmarshaling game info response: %w", err)
	}
	return gi, nil
}

func GetCompanyInfo(companyId int) (CompanyInfo, error) {
	var c CompanyInfo
	body := []byte(fmt.Sprintf("fields name,url; where id = %d;", companyId))
	resp, err := runRequest(companyUrl, body)
	if err != nil {
		return c, fmt.Errorf("Error while getting company info: %s", err.Error())
	}
	if err := json.Unmarshal(resp, &c); err != nil {
		return c, fmt.Errorf("Error unmarshaling company info response: %w", err)
	}
	return c, nil
}

func GetGameGenres(gnId []int) (Genre, error) {
	var g Genre
	reqStr := fmt.Sprintf("fields name; where id = (%s);", getStrIds(gnId))
	resp, err := runRequest("https://api.igdb.com/v4/genres", []byte(reqStr))
	if err != nil {
		return g, fmt.Errorf("Error while getting game genre: %s", err.Error())
	}
	if err := json.Unmarshal(resp, &g); err != nil {
		return g, fmt.Errorf("Error unmarshaling game genre response: %w", err)
	}

	return g, nil
}

func GetPlatformsLogos(pIds []int) (Image, error) {
	var logos Image
	reqStr := fmt.Sprintf("fields height,width,image_id,url; where id = (%s);", getStrIds(pIds))
	resp, err := runRequest("https://api.igdb.com/v4/platform_logos", []byte(reqStr))
	if err != nil {
		return logos, fmt.Errorf("Error while getting platforms logos: %s", err.Error())
	}
	if err := json.Unmarshal(resp, &logos); err != nil {
		return logos, fmt.Errorf("Error unmarshaling platforms logos response: %w", err)
	}
	return logos, nil
}

func GetGameCover(coverId int) (Image, error) {
	var i Image
	reqStr := fmt.Sprintf("fields height,width,image_id,url; where id = %d;", coverId)
	resp, err := runRequest("https://api.igdb.com/v4/covers", []byte(reqStr))
	if err != nil {
		return i, fmt.Errorf("Error while getting game cover: %s", err.Error())
	}
	if err := json.Unmarshal(resp, &i); err != nil {
		return i, fmt.Errorf("Error unmarshaling game cover response: %w", err)
	}
	return i, nil
}

func runRequest(url string, body []byte) ([]byte, error) {
	r, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("cannot create request: %w", err)
	}

	clientId := os.Getenv("IGDBCLIENTID")
	token := os.Getenv("IGDBTOKEN")

	if clientId == "" || token == "" {
		return nil, fmt.Errorf("No IGDB ClientID or Token")
	}

	r.Header.Set("Client-ID", clientId)
	r.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	resp, err := http.DefaultClient.Do(r)
	if err != nil {
		return nil, fmt.Errorf("cannot do request: %w", err)
	}
	defer resp.Body.Close()
	rData, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("cannot read request response: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("response status: %s - %s", resp.Status, string(rData))
	}

	return rData, nil
}

func getStrIds(arr []int) string {

	var b strings.Builder
	for _, i := range arr {
		if b.String() != "" {
			b.WriteString(",")
		}
		b.WriteString(fmt.Sprintf("%d", i))
	}

	return b.String()
}
