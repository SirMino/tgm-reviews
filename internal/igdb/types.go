package igdb

type GameSearch []struct {
	ID int `json:"id"`
	FirstReleaseDate int `json:"first_release_date"`
}
type CompanyInfo []struct {
	ID int `json:"id"`
	Name string `json:"name"`
	URL string `json:"url"`
}
type Image []struct {
	ID      int    `json:"id"`
	Height  int    `json:"height"`
	ImageID string `json:"image_id"`
	URL     string `json:"url"`
	Width   int    `json:"width"`
}
type Genre []struct {
	ID int `json:"id"`
	Name string `json:"name"`
}
type GameInfo []struct {
	ID    int `json:"id"`
	Cover struct {
		ID      int    `json:"id"`
		ImageID string `json:"image_id"`
		URL     string `json:"url"`
	} `json:"cover"`
	FirstReleaseDate int `json:"first_release_date"`
	Genres           []struct {
		ID   int    `json:"id"`
		Name string `json:"name"`
	} `json:"genres"`
	InvolvedCompanies []struct {
		ID        int  `json:"id"`
		Company   int  `json:"company"`
		Developer bool `json:"developer"`
		Publisher bool `json:"publisher"`
	} `json:"involved_companies"`
	Name      string `json:"name"`
	Platforms []struct {
		ID           int    `json:"id"`
		Abbreviation string `json:"abbreviation"`
		Name         string `json:"name"`
		PlatformLogo int    `json:"platform_logo"`
	} `json:"platforms"`
	Screenshots []struct {
		ID      int    `json:"id"`
		ImageID string `json:"image_id"`
		URL     string `json:"url"`
	} `json:"screenshots"`
}
type Platforms []struct {
	ID int `json:"id"`
	Name string `json:"name"`
	PlatformLogo int `json:"platform_logo"`
}
