package server

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"mi.no/tgm-reviews/internal/db"
	"mi.no/tgm-reviews/internal/handlers"
	"mi.no/tgm-reviews/internal/ui"
)

type Server struct {
	Echo    *echo.Echo
	Storage db.Storage
	Keys [][]byte
	dev     bool
}

func CreateServer(dev bool) (*Server, error) {

	var s Server

	db, err := db.NewTursoDB(dev)
	if err != nil {
		return &s, err
	}

	store := sessions.NewCookieStore(securecookie.GenerateRandomKey(32))

	s.Storage = db
	s.dev = dev
	s.Keys = make([][]byte, 0)

	s.Echo = echo.New()
	ui.NewHtmxRenderer(s.Echo, "public/*.html")
	s.Echo.Logger.SetLevel(log.DEBUG)

	// middleware
	s.Echo.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set("__db", s.Storage)
			c.Set("__keys", s.Keys)
			return next(c)
		}
	})
	s.Echo.Use(middleware.Recover())
	s.Echo.Use(middleware.BodyLimit("35K"))
	s.Echo.Use(middleware.Secure())
	s.Echo.Use(middleware.TimeoutWithConfig(middleware.TimeoutConfig{
		Timeout: 5 * time.Second,
	}))
	s.Echo.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: `{"time":"${time_rfc3339_nano}","remote_ip":"${remote_ip}",` +
			`"host":"${host}","method":"${method}","uri":"${uri}",` +
			`"status":${status},"error":"${error}","latency_human":"${latency_human}"` +
			`` + "\n",
		CustomTimeFormat: "2006-01-02 15:04:05.00000",
	}))
	s.Echo.Use(session.Middleware(store))

	// routes
	s.Echo.GET("/", handlers.Root)
	s.Echo.GET("/search", handlers.Search)
	s.Echo.GET("/game", handlers.Game)
	s.Echo.GET("/review", handlers.GetReview)
	s.Echo.GET("/newauthor", handlers.NewAuthorForm)
	s.Echo.POST("/author", handlers.NewAuthor)
	s.Echo.GET("/login", handlers.LoginForm)
	s.Echo.POST("/login", handlers.Login)
	s.Echo.GET("/gamecover", handlers.GameCover)
	s.Echo.GET("/gamedetails", handlers.GameDetails)
	s.Echo.Static("/dist", "dist")

	return &s, nil
}

func (s *Server) Start() {

	defer s.Storage.Close()

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	go func() {
		if err := s.Echo.Start(":" + os.Getenv("PORT")); err != nil && err != http.ErrServerClosed {
			s.Echo.Logger.Fatal("shutting down server")
		}
	}()

	<-ctx.Done()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := s.Echo.Shutdown(ctx); err != nil {
		s.Echo.Logger.Fatal(err)
	}
}

// func NewServer() {
// 	e := echo.New()
// 	ui.NewHtmxRenderer(e, "public/*.html")
//
// 	e.Logger.SetLevel(log.DEBUG)
//
// 	db, err := db.NewSqlite(false)
// 	if err != nil {
// 		log.Panic(err)
// 	}
// 	defer db.Close()
//
// 	// middleware
// 	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
// 		return func(c echo.Context) error {
// 			c.Set("__db", db)
// 			return next(c)
// 		}
// 	})
// 	e.Use(middleware.Recover())
// 	e.Use(middleware.BodyLimit("35K"))
// 	e.Use(middleware.Secure())
// 	e.Use(middleware.TimeoutWithConfig(middleware.TimeoutConfig{
// 		Timeout: 5 * time.Second,
// 	}))
// 	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
// 		Format: `{"time":"${time_rfc3339_nano}","remote_ip":"${remote_ip}",` +
// 			`"host":"${host}","method":"${method}","uri":"${uri}",` +
// 			`"status":${status},"error":"${error}","latency_human":"${latency_human}"` +
// 			`` + "\n",
// 		CustomTimeFormat: "2006-01-02 15:04:05.00000",
// 	}))
//
// 	// routes
// 	e.GET("/", handlers.Root)
// 	e.GET("/search", handlers.Search)
// 	e.GET("/game", handlers.Game)
// 	e.Static("/dist", "dist")
//
// 	//graceful shutdown
// 	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
// 	defer stop()
//
// 	go func() {
// 		if err := e.Start(":" + os.Getenv("SERVERPORT")); err != nil && err != http.ErrServerClosed {
// 			e.Logger.Fatal("shutting down server")
// 		}
// 	}()
//
// 	<-ctx.Done()
// 	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
// 	defer cancel()
// 	if err := e.Shutdown(ctx); err != nil {
// 		e.Logger.Fatal(err)
// 	}
// }
