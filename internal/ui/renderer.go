package ui

import (
	"io"
	"text/template"

	"github.com/labstack/echo/v4"
)

type HtmxTemplate struct {
	Templates *template.Template
}

func (t *HtmxTemplate) Render(w io.Writer, name string, data any, c echo.Context) error {
	return t.Templates.ExecuteTemplate(w, name, data)
}

func NewHtmxRenderer(e *echo.Echo, paths ...string) {
	tmpl := &template.Template{}
	for i := range paths {
		template.Must(tmpl.ParseGlob(paths[i]))
	}
	t := newTemplate(tmpl)
	e.Renderer = t
}

func newTemplate(templates *template.Template) echo.Renderer {
	return &HtmxTemplate{
		Templates: templates,
	}
}
